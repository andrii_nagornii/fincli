package com.example.client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AddCategoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        Spinner spinner = findViewById(R.id.spinner_category_type);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, new String[]{"", "PLUS", "MINUS"});

        spinner.setAdapter(adapter);
    }

    public void onButtonClick(View v) {
        RequestQueue queue = Volley.newRequestQueue(this);

        String ip = getSharedPreferences("store", Context.MODE_PRIVATE).getString("ip", "");
        String url = "https://"+ ip +":8080/add_category";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    System.out.println("TEST POST");
                }, error -> System.out.println(error.toString())) {

            public byte[] getBody() throws AuthFailureError {
                TextView tw = findViewById(R.id.textView);
                Spinner spinner = findViewById(R.id.spinner_category_type);
                String selectedType = spinner.getSelectedItem().toString();

                int categoryType = 0;
                if (selectedType.equals("PLUS")) {
                    categoryType = 1;
                } else if (selectedType.equals("MINUS")) {
                    categoryType = -1;
                }

                String httpPostBody=tw.getText() + ";" + categoryType + ";" + "-";

                return httpPostBody.getBytes();
            }

        };

        queue.add(stringRequest);
        finish();
    }
}