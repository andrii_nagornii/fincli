package com.example.client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        trustAllCert();
        setContentView(R.layout.activity_main);

        EditText ipEditText = (EditText) findViewById(R.id.textView7);
        String ipOld = getSharedPreferences("store", Context.MODE_PRIVATE).getString("ip", "");
        if (ipOld.isEmpty()) {
            String ip = ipEditText.getText().toString();
            SharedPreferences sharedPref0 = getSharedPreferences("store", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor0 = sharedPref0.edit();
            editor0.putString("ip", ip);
            editor0.commit();
        } else {
            ipEditText.setText(ipOld);
        }

        ipEditText.setOnKeyListener((v, keyCode, event) -> {

            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                String newIp = ipEditText.getText().toString();

                SharedPreferences sharedPref = getSharedPreferences("store", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("ip", newIp);
                editor.commit();

                Intent intent = getIntent();
                finish();
                startActivity(intent);

                return true;
            }

            return false;
        });

        EditText editText = (EditText) findViewById(R.id.textView6);
        String loginOld = getSharedPreferences("store", Context.MODE_PRIVATE).getString("login", "");
        if (loginOld.isEmpty()) {
            String login = editText.getText().toString();
            SharedPreferences sharedPref0 = getSharedPreferences("store", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor0 = sharedPref0.edit();
            editor0.putString("login", login);
            editor0.commit();
        } else {
            editText.setText("name = " + loginOld);
        }

        editText.setOnKeyListener((v, keyCode, event) -> {

            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                String newName = editText.getText().toString();

                SharedPreferences sharedPref = getSharedPreferences("store", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("login", newName);
                editor.apply();
                editor.commit();
                editText.setText("name = " + newName);

                return true;
            }

            return false;
        });
    }

    protected void onPostResume() {
        super.onPostResume();
        getBalance();
    }

    private void getBalance() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String ip = getSharedPreferences("store", Context.MODE_PRIVATE).getString("ip", "");
        String url = "https://"+ ip +":8080/get_balance";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    TextView tv = findViewById(R.id.textView5);
                    tv.setText("Balance = " + response);
                }, error -> System.out.println(error.toString()));

        queue.add(stringRequest);
    }

    public void onButtonClick(View v) {
        Intent intent = new Intent("com.example.client.CategoriesActivity");
        startActivity(intent);
    }

    public void onButtonClickPlus(View v) {
        Intent intent = new Intent("com.example.client.PlusOperationActivity");
        startActivity(intent);
    }

    public void onButtonClickMinus(View v) {
        Intent intent = new Intent("com.example.client.MinusOperationActivity");
        startActivity(intent);
    }

    public static void trustAllCert() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        public X509Certificate[] getAcceptedIssuers() {
                            X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                            return myTrustedAnchors;
                        }

                        @Override
                        public void checkClientTrusted(X509Certificate[] certs, String authType) {}

                        @Override
                        public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}