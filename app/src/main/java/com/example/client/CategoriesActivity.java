package com.example.client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CategoriesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    protected void onPostResume() {
        super.onPostResume();
        setContentView(R.layout.activity_categories);
        RequestQueue queue = Volley.newRequestQueue(this);
        String ip = getSharedPreferences("store", Context.MODE_PRIVATE).getString("ip", "");
        String url = "https://"+ ip +":8080/get_categories";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    String[] categoriesAsStr = response.split("\\|");

                    List<String> categories = Stream.of(categoriesAsStr)
                            .filter(category -> !category.equals(""))
                            .filter(category -> !category.equals("\n"))
                            .map(category -> category + "-")
                            .flatMap(category -> Arrays.stream(category.split(";")))
                            .map(categoryType -> {
                                if (categoryType.equals("-1"))
                                    return "-";
                                else if (categoryType.equals("1")) {
                                    return "+";
                                }
                                return categoryType;

                            })
                            .collect(Collectors.toList());

                    GridView gridView = findViewById(R.id.gridview);
                    BooksAdapter booksAdapter = new BooksAdapter(this, categories);
                    gridView.setAdapter(booksAdapter);
                }, error -> System.out.println(error.toString()));

        queue.add(stringRequest);
    }

    public void onButtonClick(View v) {
        Intent intent = new Intent("com.example.client.AddCategoryActivity");
        startActivity(intent);
    }

}

class BooksAdapter extends BaseAdapter {

    private final Context mContext;

    private List<String> categories;

    public BooksAdapter(Context context, List<String> categories) {
        this.mContext = context;
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView dummyTextView = new TextView(mContext);
        dummyTextView.setTextSize(20);
        dummyTextView.setTextColor(Color.BLACK);
        dummyTextView.setText(categories.get(position));
        return dummyTextView;
    }

}