package com.example.client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PlusOperationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plus_operation);
        Spinner spinner = findViewById(R.id.spinner);

        RequestQueue queue = Volley.newRequestQueue(this);

        String ip = getSharedPreferences("store", Context.MODE_PRIVATE).getString("ip", "");
        String url = "https://"+ ip +":8080/get_categories";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    String[] categoriesAsStr = response.split("\\|");

                    String[] categories = Stream.of(categoriesAsStr)
                            .filter(categor -> !categor.isEmpty())
                            .filter(categor -> !categor.equals("\n"))
                            .map(categor -> categor.split(";"))
                            .filter(categor -> categor[1].equals("1"))
                            .map(categor -> categor[0])
                            .toArray(String[]::new);

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, categories);

                    spinner.setAdapter(adapter);

                }, error -> System.out.println(error.toString()));

        queue.add(stringRequest);

    }

    public void onButtonClick(View v) {
        RequestQueue queue = Volley.newRequestQueue(this);

        String ip = getSharedPreferences("store", Context.MODE_PRIVATE).getString("ip", "");
        String url = "https://"+ ip +":8080/add_operation   ";

        String login = getSharedPreferences("store", Context.MODE_PRIVATE).getString("login", "");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    System.out.println("TEST POST");
                }, error -> System.out.println(error.toString())) {

            public byte[] getBody() throws AuthFailureError {
                CharSequence value = ((TextView) findViewById(R.id.textView3)).getText();
                String category = ((Spinner) findViewById(R.id.spinner)).getSelectedItem().toString();

                String httpPostBody= category + ";" + value + ";" + login + ";";

                return httpPostBody.getBytes();
            }

        };

        queue.add(stringRequest);
        finish();
    }

}